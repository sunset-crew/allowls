-- name: GetUserByTheId :one
SELECT * FROM users
WHERE id = $1 LIMIT 1;

-- name: GetUserByName :one
SELECT * FROM users
WHERE name = $1 LIMIT 1;

-- name: ListUsers :many
SELECT * FROM users
ORDER BY name;

-- name: CreateUser :one
INSERT INTO users (
  name, password
) VALUES (
  $1, $2
)
RETURNING *;

-- name: DeleteUser :exec
DELETE FROM users
WHERE id = $1;

-- name: UpdateUserPassword :exec
UPDATE users SET password = $1
WHERE name = $2;

-- name: CreateContact :exec
INSERT INTO contacts (
    ip, name, email, subject, message, count
) VALUES (
    $1, $2, $3, $4, $5, $6
)
RETURNING *;

-- name: GetContact :exec
SELECT * FROM contacts
WHERE id = $1 LIMIT 1;
