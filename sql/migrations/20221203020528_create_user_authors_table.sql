-- migrate:up

CREATE TABLE users (
  id        BIGSERIAL PRIMARY KEY,
  name      text      NOT NULL,
  password  text
);

CREATE TABLE contacts (
  id        BIGSERIAL    PRIMARY KEY,
  ip        VARCHAR(65)  UNIQUE,
  name      VARCHAR(65)  NOT NULL,
  email     VARCHAR(65)  NOT NULL,
  subject   text,
  message   text,
  count     INT
);

CREATE TABLE allowlist (
  id        BIGSERIAL    PRIMARY KEY,
  ip        VARCHAR(65)  UNIQUE,
  count     INT
);

-- migrate:down
DROP TABLE users;
DROP TABLE contacts;
DROP TABLE allowlist;
