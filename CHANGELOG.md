# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Also based on](https://github.com/conventional-changelog/standard-version/blob/master/CHANGELOG.md) so decending.

## [0.1.4] - 2023-12-15
### Added
- adds code for rockylinux as it seems centos 9 container is hosed
- adds the dbmate help section to readme
- adds centos9 docker compose to test both rocky and centos9

### Removed
- removes static uri from main server

### Changed
- updates readme with more dev info

## [0.1.3] - 2022-12-09
### Added
- adds delete functionality to web ui
- adds debian package building option to the deploy section

### Changed
- changes makefile to make it easier to use

## [0.1.2] - 2022-12-09
### Added
- adds toggling of logout button at login and render

### Changed
- changes login to reload the page for now

## [0.1.1] - 2022-12-09
### Added
- adds more debug info to private ping

### Removed
- removes error catch for clean script
- removes last references to whitelist

### Changed
- changes .version to .env for docker-compose and versionupdater
- fixes formating
- fixes a bug in the docker compose file

## [0.1.0] - 2022-12-05
### Added
- adds changelog version updater and layers the centos image a bit
- adds changes to shift into an owl type cast
- adds off same as would be stop or down
- adds important env cmds to readme
- adds loadfromfile and dumptofile functions to sett struct
- adds session checking for homeweb and new private ping
- adds ip list renderer and better display ui
- adds env_file default and also cp .version to .env

### Removed
- removes number on the error key

### Changed
- changes installed packages to production version
- changes var to public for local data storage