package server

import (
	"encoding/json"
	// "errors"
	"allowls/db"
	"bytes"
	"fmt"
	"github.com/hashicorp/memberlist"
	"github.com/pborman/uuid"
	"log"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"
)

type broadcast struct {
	msg    []byte
	notify chan<- struct{}
}

type Update struct {
	Action string // set, del
	Data   map[string]interface{}
}

func (b *broadcast) Invalidates(other memberlist.Broadcast) bool {
	return false
}

func (b *broadcast) Message() []byte {
	return b.msg
}

func (b *broadcast) Finished() {
	if b.notify != nil {
		close(b.notify)
	}
}

func (s *Server) NodeMeta(limit int) []byte {
	return []byte{}
}

func (s *Server) NotifyMsg(b []byte) {
	if len(b) == 0 {
		return
	}

	switch b[0] {
	case 'd': // data
		var updates []*Update
		if err := json.Unmarshal(b[1:], &updates); err != nil {
			return
		}
		s.mtx.Lock()
		for _, u := range updates {
			for k, v := range u.Data {
				switch u.Action {
				case "set":
					s.storage.SetStr(k, v.(string))
					ProcessIP("add", k, v)
				case "del":
					s.storage.Delete(k)
					ProcessIP("remove", k, v)
				}

			}
		}
		s.mtx.Unlock()
	}
}

func (s *Server) GetBroadcasts(overhead, limit int) [][]byte {
	return s.broadcasts.GetBroadcasts(overhead, limit)
}

func (s *Server) LocalState(join bool) []byte {
	m := make(map[string]string)
	s.mtx.RLock()
	fmt.Println(" === Sharing Remote State for push/pull sync === ")
	keys, err := s.storage.Keys()
	fmt.Println(strings.Join(keys, ","))
	if err != nil {
		log.Fatal(err)
	}
	for _, x := range keys {
		m[x], err = s.storage.GetStr(x)
		if err != nil {
			log.Fatal(err)
		}
	}
	s.mtx.RUnlock()
	b, _ := json.Marshal(m)
	return b
}

func (s *Server) MergeRemoteState(buf []byte, join bool) {
	if len(buf) == 0 {
		return
	}
	if !join {
		return
	}
	var m map[string]interface{}
	if err := json.Unmarshal(buf, &m); err != nil {
		return
	}
	s.mtx.Lock()
	fmt.Println("Starting")
	fmt.Println(buf)

	if m != nil {
		fmt.Println("got sumptin")
	}
	for k, v := range m {
		ProcessIP("add", k, v)
		s.storage.SetStr(k, v.(string))
	}
	fmt.Println(s.storage.Keys())
	s.mtx.Unlock()
}

type eventDelegate struct{}

func (ed *eventDelegate) NotifyJoin(node *memberlist.Node) {
	fmt.Println("A node has joined: " + node.String())
}

func (ed *eventDelegate) NotifyLeave(node *memberlist.Node) {
	fmt.Println("A node has left: " + node.String())
}

func (ed *eventDelegate) NotifyUpdate(node *memberlist.Node) {
	fmt.Println("A node was updated: " + node.String())
}

type Options struct {
	// Unique ID of server
	ID string
	// Local address to bind to
	Address string
	// Members in the cluster
	Members []string
	// Loads the local data file
	Load bool
	// DBFile for local dumps
	DbFile string
}

type Server struct {
	Options *Options

	mtx sync.RWMutex
	// TODO pluggable storage
	// storage    map[string]interface{}
	storage    *db.Sett
	cluster    *memberlist.Memberlist
	broadcasts *memberlist.TransmitLimitedQueue
}

func (s *Server) Address() string {
	return s.cluster.LocalNode().FullAddress().Addr
}

func (s *Server) All() ([]string, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()

	//~ for k, v := range s.storage {
	//~ fmt.Println(k,v)
	//~ }
	ks, err := s.storage.Keys()
	return ks, err
}

func (s *Server) Get(key string) (interface{}, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()

	//~ if v, ok := s.storage[key]; ok {
	//~ return v, nil
	//~ }
	v, err := s.storage.GetStr(key)
	if err != nil {
		return nil, err
	}

	return v, nil
}

func (s *Server) Set(key string, val interface{}) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	err := s.storage.SetStr(key, val.(string))
	if err != nil {
		return err
	}
	ProcessIP("add", key, val)
	s.storage.Dump()
	b, err := json.Marshal([]*Update{
		{
			Action: "set",
			Data: map[string]interface{}{
				key: val,
			},
		},
	})

	if err != nil {
		return err
	}

	s.broadcasts.QueueBroadcast(&broadcast{
		msg:    append([]byte("d"), b...),
		notify: nil,
	})

	return nil
}

func (s *Server) Delete(key string) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	s.storage.Delete(key)
	s.storage.Dump()

	ProcessIP("remove", key, "d")

	b, err := json.Marshal([]*Update{{
		Action: "del",
		Data: map[string]interface{}{
			key: nil,
		},
	}})

	if err != nil {
		return err
	}

	s.broadcasts.QueueBroadcast(&broadcast{
		msg:    append([]byte("d"), b...),
		notify: nil,
	})

	return nil
}

func ProcessIP(action string, ip string, list interface{}) {
	liststr := string(list.(string))
	fmt.Println(liststr)
	fmt.Println("running command "+action)
	cmd := exec.Command("firewallwrapper.sh", action, ip)
	// cmd := exec.Command("./firewallwrapper.sh", action, ip)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		// log.Fatalf("cmd.Run() failed with %s\n", err)
		log.Printf("cmd.Run() failed with %s\n", err)
	}
}

func New(opts *Options) (*Server, error) {
	s := new(Server)
	s.storage = db.Open(opts.DbFile)
	if opts.Load {
		s.storage.Load()
	}
	c := memberlist.DefaultLocalConfig()

	// set hostname
	if len(opts.ID) == 0 {
		hostname, _ := os.Hostname()
		c.Name = hostname + "-" + uuid.NewUUID().String()
	} else {
		c.Name = opts.ID
	}

	// set address
	if len(opts.Address) > 0 {
		if h, p, err := net.SplitHostPort(opts.Address); err == nil {
			c.BindAddr = h
			c.BindPort, _ = strconv.Atoi(p)
		}
	} else {
		c.BindPort = 0
	}

	c.Events = &eventDelegate{}
	c.Delegate = s
	// c.PushPullInterval = time.Second * 60 // to make it demonstrable
	c.PushPullInterval = time.Second * 15 // to make it demonstrable
	c.ProbeInterval = time.Second * 3     // to make failure demonstrable

	m, err := memberlist.Create(c)
	if err != nil {
		return nil, err
	}

	// add members to cluster
	if len(opts.Members) > 0 {
		m.Join(opts.Members)
	}

	br := &memberlist.TransmitLimitedQueue{
		NumNodes: func() int {
			return m.NumMembers()
		},
		// RetransmitMult: 3,
		RetransmitMult: 2,
	}

	s.Options = opts
	s.cluster = m
	s.broadcasts = br
	return s, nil
}

func createKeyValuePairs(m map[string]interface{}) string {
	b := new(bytes.Buffer)
	for key, value := range m {
		fmt.Fprintf(b, "%s=\"%s\"\n", key, value)
	}
	return b.String()
}
