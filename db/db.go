package db

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"time"
	// "encoding/gob"
	"bytes"
	badger "github.com/dgraph-io/badger/v3"
)

// pulled a lot of code from https://github.com/prasanthmj/sett

const (
	STRUCT_TYPE = 1
	STRING_TYPE = 2
)

var (
	DefaultIteratorOptions = badger.DefaultIteratorOptions
)

type Sett struct {
	db        *badger.DB
	ttl       time.Duration
	keyLength int
	DbFile    string
}

type SettItem struct {
	fullKey string
	s       *Sett
	txn     *badger.Txn
	unlock  bool
}

type genericContainer struct {
	V interface{}
}

// NewSettItem
func NewSettItem(s *Sett, txn *badger.Txn, key string) *SettItem {
	return &SettItem{fullKey: key, s: s, txn: txn, unlock: false}
}

// SetStringValue
func (si *SettItem) SetStringValue(val string) error {
	if !si.unlock && si.IsLocked() {
		return fmt.Errorf("The item with key %s is locked. Can't update now", si.fullKey)
	}
	e := badger.NewEntry([]byte(si.fullKey), []byte(val))

	err := si.setEntry(e, STRING_TYPE)
	return err
}

// Unluck
func (si *SettItem) Unlock(u bool) {
	si.unlock = u
}

// IsLocked
func (si *SettItem) IsLocked() bool {
	item, err := si.txn.Get([]byte(si.fullKey))
	if err != nil {
		return false
	}
	if (item.UserMeta() & 0x80) != 0 {
		return true
	}
	return false
}

// SetEntry
func (si *SettItem) setEntry(e *badger.Entry, vtype byte) error {
	if si.s.ttl > 0 {
		e.WithTTL(si.s.ttl)
	}
	e.WithMeta(vtype)
	return si.txn.SetEntry(e)
}

// GetStringValue
func (si *SettItem) GetStringValue() (string, error) {
	item, err := si.txn.Get([]byte(si.fullKey))
	if err != nil {
		return "", err
	}
	var val []byte
	val, err = item.ValueCopy(nil)
	if err != nil {
		return "", err
	}
	return string(val), nil
}

// Delete
func (si *SettItem) Delete() error {
	if !si.unlock && si.IsLocked() {
		return fmt.Errorf("The item with key %s is locked. Can't delete now", si.fullKey)
	}

	return si.txn.Delete([]byte(si.fullKey))
}

// Open is constructor function to create badger instance,
// configure defaults and return struct instance
func Open(dbfile string) *Sett {
	s := Sett{}
	s.DbFile = dbfile
	opt := badger.DefaultOptions("").WithInMemory(true)
	db, err := badger.Open(opt)
	if err != nil {
		log.Fatal("Open: create or open failed")
	}
	s.db = db
	return &s
}

// SetStr
func (s *Sett) SetStr(key string, val string) error {
	var err error
	err = s.db.Update(func(txn *badger.Txn) error {
		si := NewSettItem(s, txn, key)
		return si.SetStringValue(val)
	})
	return err
}

// GetStr returns value of queried key from badger
func (s *Sett) GetStr(key string) (string, error) {
	var val string
	var err error
	err = s.db.View(func(txn *badger.Txn) error {
		si := NewSettItem(s, txn, key)
		val, err = si.GetStringValue()
		return err
	})
	if err != nil {
		return "", err
	}
	return val, nil
}

// WithTTL
func (s *Sett) WithTTL(d time.Duration) *Sett {
	s.ttl = d
	return s
}

// deleteItem
func (s *Sett) deleteItem(key string, unlock bool) error {
	err := s.db.Update(func(txn *badger.Txn) error {
		sit := NewSettItem(s, txn, key)
		sit.Unlock(unlock)
		return sit.Delete()
	})
	return err
}

// Delete removes a key and its value from badger instance
func (s *Sett) Delete(key string) error {
	return s.deleteItem(key, false)
}

// Keys lists the keys of the kv
func (s *Sett) Keys(filter ...string) ([]string, error) {
	var result []string
	var err error
	err = s.db.View(func(txn *badger.Txn) error {
		var fullFilter string
		it := txn.NewIterator(DefaultIteratorOptions)
		defer it.Close()

		if len(filter) > 1 {
			return errors.New("Can't accept more than one filters")
		}

		if len(filter) == 1 {
			fullFilter += filter[0]
		}

		for it.Seek([]byte(fullFilter)); it.ValidForPrefix([]byte(fullFilter)); it.Next() {
			item := it.Item()
			k := string(item.Key())

			result = append(result, k)
		}
		return err
	})
	return result, err
}

// Dump lists the keys of the kv
func (s *Sett) Dump(filter ...string) (map[string]string, error) {
	result := make(map[string]string)
	var err error
	err = s.db.View(func(txn *badger.Txn) error {
		var fullFilter string
		it := txn.NewIterator(DefaultIteratorOptions)
		defer it.Close()

		if len(filter) > 1 {
			return errors.New("Can't accept more than one filters")
		}

		if len(filter) == 1 {
			fullFilter += filter[0]
		}

		for it.Seek([]byte(fullFilter)); it.ValidForPrefix([]byte(fullFilter)); it.Next() {
			item := it.Item()
			k := string(item.Key())
			v, err := item.ValueCopy(nil)
			if err != nil {
				log.Fatal(err)
			}
			result[k] = string(v)
		}
		return err
	})
	jsonStr, err := json.Marshal(result)
	s.DumpToFile(string(jsonStr))
	return result, err
}

// Dump lists the keys of the kv
func (s *Sett) Load(filter ...string) {
	var result map[string]string
	var err error
	rawdata := s.LoadFromFile()
	if err = json.Unmarshal(rawdata, &result); err != nil {
		panic(err)
	}
	for k, v := range result {
		s.SetStr(k, v)
	}
}

func (s *Sett) DumpToFile(data string) {
	f, err := os.Create(s.DbFile)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	wr := bufio.NewWriter(f)
	wr.WriteString(data)
	wr.Flush()
	fmt.Println("data written")
}

func (s *Sett) LoadFromFile() []byte {
	var buffer bytes.Buffer
	f, err := os.Open(s.DbFile)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		// fmt.Println(scanner.Text())
		buffer.WriteString(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return buffer.Bytes()
}

func TestDb() {
	db := Open("ipdb-test.dat")
	db.SetStr("127.0.0.1", "allowlist")
	val, err := db.GetStr("127.0.0.1")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(val)
	err = db.Delete("127.0.0.1")
	if err != nil {
		log.Fatal(err)
	}
	val, err = db.GetStr("127.0.0.1")
	if err != nil {
		log.Println("can't find 127.0.0.1, on purpose\n", err)
	}
	db.SetStr("4.3.3.3", "denylist")
	db.SetStr("10.34.2.1", "allowlist")
	db.SetStr("123.2.2.2", "allowlist")
	results, err := db.Keys()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(results)
	db.Dump()
	db = Open("ipdb-test.dat")
	val, err = db.GetStr("127.0.0.1")
	if err != nil {
		log.Println("can't find 127.0.0.1, on purpose again\n", err)
	}
	db.Load()
	results, err = db.Keys()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(results)
}
