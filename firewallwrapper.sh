#!/bin/bash

set -Eeuo pipefail

cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1

trap cleanup SIGINT SIGTERM ERR EXIT

usage() {
  cat <<EOF
Usage: $(basename "$0") [-h] {action} {ip}

Actions:
    add
    remove

Set an IP on allowlist in the various areas: firewalld, iptables, nft, pf
You can set the type by setting the ENV var.

LIST_TYPE=firewalld (default)

Available options:

-h, --help      Print this help and exit

Try to use it like this: 

    $ firewallwrapper 127.0.0.1
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOCOLOR='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOCOLOR='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}
setup_colors

die(){
  # example: die another day 007
  # always end with a number
  # 0 == success, 1+ == fail
  # split string
  line=( "$@" )
  end=${#line[@]}-1
  error_msg="${line[@]:0:$end}"
  msg "${RED}${error_msg}${NOCOLOR}"
  usage
  exit ${line[@]: -1}
}

msg() {
  echo >&2 -e "${1-}"
}

#~ die() {
    
  #~ local msg=$1
  #~ local code=${2-1} # default exit status 1
  #~ msg "$msg"
  #~ exit "$code"
#~ }

check_args() {
    if [ $# -ne 2 ]; 
        then usage
    fi
}

# check_args

parse_params() {
  # default values of variables set from params

  list_type=${WHITELIST_TYPE:-firewalld}
  primary_int=${PRIMARY_INTERFACE:-eth0} # vio0
  primary_zone=${PRIMARY_ZONE:-permitipa}
  priv_ports=(6379 5432 3306 8080)
  pub_ports=(22 80 443 25 587 465)
  possible_list_types=("firewalld" "iptables" "nft" "pf")

  while :; do
    case "${1-}" in
    -l | --list-type)
      if [[ ! " ${possible_list_types[*]} " =~ " ${value} " ]]; then
        die ${value} not found in command list 3
      fi
      list_type=${2-}
      echo "$list_type"
      shift
      ;;

    -i | --primary-interface)
      primary_int=${2-}
      local_raw=$(ip a)
      if grep -q "$primary_int" <<< $local_raw; then
        die not found in command list 4
      fi
      shift
      ;;

    -z | --primary-zone)
      if [ "$list_type" != "firewalld" ]; then 
        die zones only work on firewalld 5
      fi
      primary_zone=${2-}
      shift
      ;;

    -h | --help)
      usage
      ;;

    --no-color)
      NO_COLOR=1
      ;;

    *)
      break
      ;;
    esac
    shift
  done

  args=("$@")
  if [[ ${#args[@]} > 0 ]]; then 
     echo $args
  fi
  
  # check required params and arguments
  #[[ -z "${param-}" ]] && die "Missing required parameter: param"
  [[ ${#args[@]} -ne 2 ]] && die Missing script arguments you need two 23

  return 0
}

parse_params "$@"


# script logic here

#~ msg "${RED}Read parameters:${NOCOLOR}"
#~ msg "${CYAN}lsososo parameters:${NOCOLOR}"
#~ msg "${GREEN}lsososo parameters:${NOCOLOR}"
#~ msg "- arguments: ${args[*]-}"

# die this is wonky this scs 255

add-firewalld-cmd(){
    ip=$1
    # [root@ipa ~]# firewall-cmd --zone=permitipa --add-source=10.1.1.1 --permanent
    # AddIPtoZone <zone> <ip>
    msg "${CYAN}firewall-cmd --zone=$primary_zone --add-source=$ip --permanent${NOCOLOR}"
    [ -f "/usr/sbin/firewalld" ] && firewall-cmd --zone=$primary_zone --add-source=$ip --permanent || echo "catch Error"
    msg "${CYAN}firewall-cmd --reload${NOCOLOR}"
    [ -f "/usr/sbin/firewalld" ] && firewall-cmd --reload
    echo "Adds $ip"
}
# RemoveIPFromZone(){
remove-firewalld-cmd(){
    ip=$1
    # [root@ipa ~]# firewall-cmd --zone=permitipa --add-source=10.1.1.1 --permanent
    # RemoveIPFromZone <zone> <ip>
    msg "${CYAN}firewall-cmd --zone=$primary_zone --remove-source=$ip --permanent${NOCOLOR}"
    [ -f "/usr/sbin/firewalld" ] && firewall-cmd --zone=$primary_zone --remove-source=$ip --permanent || echo "catch Error"
    msg "${CYAN}firewall-cmd --reload${NOCOLOR}"
    [ -f "/usr/sbin/firewalld" ] && firewall-cmd --reload
    echo "Removes $ip"
}

buildzone-firewalld-cmd(){
    ip=$1
# [root@ipa ~]# cat /etc/firewalld/zones/permitipa.xml
    cat <<EOT > /etc/firewalld/zones/${primary_zone}.xml
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <service name="freeipa-ldap"/>
  <service name="freeipa-ldaps"/>
  <service name="dns"/>
  <service name="ntp"/>
  <service name="ssh"/>
  <service name="http"/>
  <service name="https"/>
  <port port="8080" protocol="tcp"/>
  <source address="${ip}"/>
</zone>
EOT
    firewall-cmd --zone=public --remove-service=ssh --permanent
    firewall-cmd --reload
    firewall-cmd --zone=${primary_zone} --add-interface ${primary_int} --permanent
    firewall-cmd --reload
    firewall-cmd --zone=public --add-interface ${primary_int} --permanent
    firewall-cmd --reload
}

add-iptables-cmd(){
    ip=$1
    # [root@ipa ~]# firewall-cmd --zone=permitipa --add-source=10.1.1.1 --permanent
    # AddIPtoZone <ip>
    msg "${CYAN}sudo iptables -A INPUT -p tcp -s $ip --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT${NOCOLOR}"

    for port in ${priv_ports}
    do
        echo $port
        iptables -A INPUT -p tcp -s $ip --dport ${port} -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
        # sudo iptables -A OUTPUT -p tcp --sport ${port} -m conntrack --ctstate ESTABLISHED -j ACCEPT
    done

}

# RemoveIPFromZone(){
remove-iptables-cmd(){
    ip=$1
    # [root@ipa ~]# firewall-cmd --zone=permitipa --add-source=10.1.1.1 --permanent
    # RemoveIPFromZone <zone> <ip>
    msg "${CYAN}firewall-cmd --zone=${primary_zone} --remove-source=$ip --permanent${NOCOLOR}"
    msg "${CYAN}firewall-cmd --reload${NOCOLOR}"
    for port in ${priv_ports}
    do
        echo $port
        sudo iptables -D INPUT -p tcp -s $ip --dport ${port} -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
        # sudo iptables -A OUTPUT -p tcp --sport ${port} -m conntrack --ctstate ESTABLISHED -j ACCEPT
    done
}

buildzone-iptables-cmd(){
    ip=$1
# [root@ipa ~]# cat /etc/firewalld/zones/permitipa.xml
    cat <<EOT > /etc/sysconfig/iptables.config
-P INPUT DROP
-P FORWARD DROP
-P OUTPUT ACCEPT
-N ICMP
-N TCP
-N UDP
-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m conntrack --ctstate INVALID -j DROP
-A INPUT -p udp -m conntrack --ctstate NEW -j UDP
-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP
-A INPUT -p icmp -m conntrack --ctstate NEW -j ICMP
-A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
-A INPUT -p tcp -j REJECT --reject-with tcp-reset
-A INPUT -j REJECT --reject-with icmp-proto-unreachable

EOT
    for port in ${pub_ports}
    do
        echo $port
        echo "-A INPUT -p tcp -m tcp --dport ${port} -j ACCEPT" >> /etc/sysconfig/iptables.config
    done
    iptables-restore < /etc/sysconfig/iptables.config
}

add-nft-cmd() {
    ip=$1
    nft add element inet filter allowlist { $ip }
}

remove-nft-cmd() {
    ip=$1
    nft delete element inet filter allowlist { $ip }
}

buildzone-nft-cmd() {
    ip=$1
    cat <<'EOT' > /etc/nftables.conf
table inet filter {                                                                                                                                         
        set allowlist {
                type ipv4_addr                                                                                                                                                                         
                elements = { $ip }                                                                                                           

        chain input {                                                                                                                                               
                type filter hook input priority filter; policy drop;                                                                                                                                   
                ct state established,related accept                                                                                                                                                    
                iifname "lo" accept                                                                                                                                                                    
                icmp type echo-request accept                                                                                                                                                          
                                                                                                                                                        
                tcp dport { 22, 443, 4222 } accept                                                                                                                                                     
                tcp dport { 80, 4222, 8081 } ip saddr @allowlist accept
        }

        chain forward {                                                                                                                                                     
                type filter hook forward priority filter; policy accept;                                                                                                                               
        }                                                                                                           

        chain output {                                                                                                                                                  
                type filter hook output priority filter; policy accept;                                                                                                                                
        }                                                                                                           
} 
EOT
    systemctl restart nftables
    #~ for nip in ips
    #~ do
        #~ add-nft-cmd $nip
    #~ done
}

add-pf-cmd(){
    ip=$1
    # pfctl -t goodguys -T add 203.0.113.0/24
    pfctl -t goodguys -T add $ip
}

remove-pf-cmd(){
    ip=$1
    # pfctl -t goodguys -T add 203.0.113.0/24
    pfctl -t goodguys -T delete $ip
}

buildzone-pf-cmd(){
    ip=$1
    cat <<'EOT' > /etc/pf.conf
#       $OpenBSD: pf.conf,v 1.55 2017/12/03 20:40:04 sthen Exp $
#
# See pf.conf(5) and /etc/examples/pf.conf

inbound_pub_tcp = "{ https, http, submission, smtp, smtps }"
inbound_priv_udp = "{ 51820 }"
inbound_priv_tcp = "{ imaps, imap, ssh, 1977, 53589, 5432 }"

set skip on lo

block all
block return    # block stateless traffic

table <badhosts> persist
table <bruteforce> persist
EOT

    cat <<EOT >> /etc/pf.conf
table <goodguys> { $ip }

the_bad_guys = "{<badhosts> <bruteforce>}"

icmp_types = "{ 0, 8, 3, 4, 11, 30 }"

pass in quick inet proto icmp icmp-type \$icmp_types
pass in quick inet6 proto icmp6

# pass in on wg0
# # pass in inet proto udp from any to any port 51820
# pass out on egress inet from (wg0:network) nat-to (vio0:0)

pass in quick on egress proto tcp from <goodguys> to port \$inbound_priv_tcp
pass in quick on egress proto udp from <goodguys> to port \$inbound_priv_udp
pass in on egress proto tcp to port \$inbound_pub_tcp keep state (max-src-conn 65, max-src-conn-rate 15/5, overload <bruteforce> flush global)
pass in quick on egress proto tcp from <goodguys> to port \$inbound_pub_tcp
EOT

cat <<EOT >> /etc/pf.conf
# block everyone but the ips listed
block on $primary from \$the_bad_guys to any

# By default, do not permit remote connections to X11
block return in on \! lo0 proto tcp to port 6000:6010

# Port build user does not need network
block return out log proto \{tcp udp\} user _pbuild

pass out
EOT
    pfctl -f /etc/pf.conf
}

${args[0]}-${list_type}-cmd ${args[1]}
