#!/usr/bin/env bash

DNS_SERVER="10.45.1.1"

kill_all() {
        kill ${PID_FIREWALLD}
        kill ${PID_DBUS}
}

# let dbus run as "root"
sed -i '/<user>/d' /usr/share/dbus-1/system.conf
# for dbus socket
mkdir -p /run/dbus

dbus-daemon --system --nofork --nopidfile --nosyslog &
PID_DBUS=$!
sleep 1

firewalld --nofork --nopid &

sleep 5

hosts_str="$1"
ports_str="$2"

IFS=',' read -r -a hosts_arr <<< "$hosts_str"
IFS=',' read -r -a ports_arr <<< "$ports_str"


firewall-cmd --new-zone=permitipa --permanent

for ip in ${hosts_arr[@]}
do 
    firewall-cmd --zone=permitipa --add-source=${ip} --permanent
done 

for port in ${ports_arr[@]}
do
    echo $port
    firewall-cmd --zone permitipa --add-port ${port}/tcp --permanent
done

firewall-cmd --zone=permitipa --add-service={http,https} --permanent
firewall-cmd --zone=public --remove-service={ssh,cockpit,dhcpv6-client} --permanent
firewall-cmd --reload
firewall-cmd --zone=permitipa --add-interface eth0 --permanent
firewall-cmd --reload
firewall-cmd --zone=public --add-interface eth0 --permanent
firewall-cmd --reload

echo "nameserver ${DNS_SERVER}" > /etc/resolv.conf

PID_FIREWALLD=$!

trap kill_all TERM

wait -n
exit $?
