#!/bin/bash 

set -Eeuo pipefail

cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1

[ -d "build" ] && sudo rm -rf build/

exit 0
