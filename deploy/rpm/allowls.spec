Name: allowls
Summary: 100 percent pure owl
Version: v0.1.4
Release: 1%{?dist}
License: BSD
URL: http://unherd.info/info
Source0: %{name}-%{version}.tar.gz
%undefine _missing_build_ids_terminate_build
%description 
A allowls cluster. So many owls!!!

%prep 
%setup -q

%build 
go build

%install
install -m 0755 -d $RPM_BUILD_ROOT/usr/local/bin
install -m 0755 allowls $RPM_BUILD_ROOT/usr/local/bin/allowls
install -m 0755 firewallwrapper.sh $RPM_BUILD_ROOT/usr/local/bin/firewallwrapper.sh

%clean 
rm -rf %{buildroot}
%undefine __check_files


%pre
if [ $1 == 2 ]; then
  # this only runs during an update
  echo "update"
fi
if [ $1 == 1 ]; then
  echo "install"
fi

#%post 
#/sbin/ldconfig

%preun

%postun
if [  $1 == 1 ]; then
  echo "upgrade"
fi
if [  $1 == 0 ]; then
  echo "uninstall"
fi



%files
%defattr(-,root,root,-)
/usr/local/bin/firewallwrapper.sh
/usr/local/bin/allowls

%doc README.md

%changelog
* Tue Nov 29 2022 Joe Siwiak <kf4jas@gmail.com>

