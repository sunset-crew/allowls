#  AllOwls

This was made with 100% owls. 

A distributed in-memory key-value store of ip addresses for allowlists built using [hashicorp/memberlist](https://github.com/hashicorp/memberlist) with HTTP API

## Install

```shell
./deploy/rpm/build.sh
dnf install deploy/rpm/build/RPMS/x86_64/allowlist-1-v0.1.0.el9.x86_64.rpm
```

## Docker 

```
cd deploy/rpm
./clean.sh
./build.sh
cd ../..
docker-compose up --build -d
```

## Usage

```shell
allowlist
-nodes="": comma seperated list of nodes
-address=:4001 http server host:port
-rpc_address=:4441 rpc server host:port
-load - loads local dump file into cluster
-store=ipdb.dat a file other than that
```

### Create Cluster

Start first node

```shell
allowlist --address 172.16.4.7:4005 --rpc_address 172.16.4.7:4445 --nodes 172.16.4.6:4444,172.16.4.5:4443 --load
```

Make a note of the local node address

```
Local node 172.16.4.7:60496
Listening on :4001
```

Start second node with first node as part of the nodes list

```shell
allowlist --nodes=192.168.1.64:60496 --address=:4002
```

You should see the output

```
2015/10/17 22:13:49 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.1.64:60496
Local node 192.168.1.64:60499
Listening on :4002
```

First node output will log the new connection

```shell
2015/10/17 22:13:49 [DEBUG] memberlist: TCP connection from: 192.168.1.64:60500
2015/10/17 22:13:52 [DEBUG] memberlist: Initiating push/pull sync with: 192.168.1.64:60499
```

## HTTP API

- /get - get a value
- /set - set a value
- /del - delete a value

Query params expected are `key` and `val`

```shell
# add
curl "http://localhost:4001/set?key=foo&val=bar"

# get
curl "http://localhost:4001/get?key=foo"

# delete
curl "http://localhost:4001/del?key=foo"
```

## Development

### set ENVIRONMENT


```
set -a
source .env
source .version
set +a
```


### DB migrations via dbmate

```
-d "./sql/migrations"
dbmate --help    # print usage help
dbmate new       # generate a new migration file
dbmate up        # create the database (if it does not already exist) and run any pending migrations
dbmate create    # create the database
dbmate drop      # drop the database
dbmate migrate   # run any pending migrations
dbmate rollback  # roll back the most recent migration
dbmate down      # alias for rollback
dbmate status    # show the status of all migrations (supports --exit-code and --quiet)
dbmate dump      # write the database schema.sql file
dbmate wait      # wait for the database server to become available
```


https://github.com/satrobit/memberlist-healthcheck-example/blob/master/main.go

### sqlc 

edit schema.sql
(add table)
edit query.sql
Get,Create,List,Update,Delete

```
sqlc generate
```
