VERSION := 0.1.4


deb: cleandeb
	./deploy/deb/build.sh

rpm: cleanrpm
	./deploy/rpm/clean.sh
	./deploy/rpm/build.sh

up:
	docker compose up -d

down:
	docker compose down

cleanrpm:
	-./deploy/rpm/clean.sh

cleandeb:
	-./deploy/deb/clean.sh

patch:
	git aftermerge patch || exit 1

minor:
	git aftermerge minor || exit 1

major:
	git aftermerge major || exit 1
