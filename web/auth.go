package web

import (
	"os"
	// "fmt"
	dapi "allowls/api"
	"database/sql"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
	chtml "html"
	"net/http"
	"strconv"
	"strings"
)

const UserKey = "dsmsoUser"

type UserInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GetUserByID(c *gin.Context) {
	id := c.Param("id")
	intid, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	db_url := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	queries := dapi.New(db)
	user, err := queries.GetUserByTheId(c, intid)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"result": user})
	return
}

func GetUsers(c *gin.Context) {
	db_url := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	queries := dapi.New(db)

	// list all users
	users, err := queries.ListUsers(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"users": users})
	return
}

func PostUser(c *gin.Context) {
	var user dapi.User
	db_url := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	queries := dapi.New(db)
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	insertedUser, err := queries.CreateUser(c, dapi.CreateUserParams{
		Name:     user.Name,
		Password: user.Password,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"id": insertedUser.ID, "message": "added"})
}

func Login(c *gin.Context) {
	var user UserInput
	db_url := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	queries := dapi.New(db)
	session := sessions.Default(c)
	// username := c.PostForm("username")
	// password := c.PostForm("password")
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if strings.Trim(user.Username, " ") == "" || strings.Trim(user.Password, " ") == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Parameters can't be empty"})
		return
	}
	// hashedPassword, err := HashPassword(user.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newUsername := chtml.EscapeString(strings.TrimSpace(user.Username))
	foundUser, err := queries.GetUserByName(c, newUsername)
	if err != nil {
		// err.Error() - sql: no rows in result set
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Login Failed"})
		return
	}
	if CheckPasswordHash(user.Password, foundUser.Password.String) {
		session.Set(UserKey, foundUser.Name)
		cookie := &http.Cookie{
			Name:   "wonky",
			Value:  "Youve been wonked",
			MaxAge: 300,
		}
		http.SetCookie(c.Writer, cookie)
		if err := session.Save(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error 6": "Failed to save session"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"id": foundUser.ID, "message": "logged in"})
		return
	}
	c.JSON(http.StatusInternalServerError, gin.H{"error 6": "Failed to save session"})
	return
}

func UpdateUser(c *gin.Context) {
	var user UserInput
	db_url := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	queries := dapi.New(db)
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	hashedPassword, err := HashPassword(user.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err = queries.UpdateUserPassword(c, dapi.UpdateUserPasswordParams{
		Name: user.Username,
		Password: sql.NullString{
			String: string(hashedPassword),
			Valid:  true,
		},
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"result": user, "message": "updated"})
}

func DeleteUserByID(c *gin.Context) {
	id := c.Param("id")
	intid, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	db_url := os.Getenv("DATABASE_URL")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	queries := dapi.New(db)
	err = queries.DeleteUser(c, intid)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": id + " deleted"})
}

func AuthRequired(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(UserKey)
	if user == nil {
		// Abort the request with the appropriate error code
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
		return
	}
	// Continue down the chain to handler etc
	c.Next()
}

func Logout(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(UserKey)
	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid session token"})
		return
	}
	session.Delete(UserKey)
	if err := session.Save(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save session"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Successfully logged out"})
}

func Register(c *gin.Context) {
	var input UserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error 1": err.Error()})
		return
	}
	db_url := os.Getenv("DATABASE_URL")
	// fmt.Println("dd",db_url,"aa")
	db, err := sql.Open("postgres", db_url)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error 2": err.Error()})
		return
	}
	queries := dapi.New(db)
	hashedPassword, err := HashPassword(input.Password)
	newUsername := chtml.EscapeString(strings.TrimSpace(input.Username))
	newUser, err := queries.CreateUser(c, dapi.CreateUserParams{
		Name: newUsername,
		Password: sql.NullString{
			String: string(hashedPassword),
			Valid:  true,
		},
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error 3": err.Error()})
		return
	}
	session := sessions.Default(c)
	session.Set(UserKey, newUser.Name)
	if err := session.Save(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error 4": "Failed to save session"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "registered validated!"})
}
