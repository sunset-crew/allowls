package web

import (
	// "os"
	// "fmt"
	"embed"
	"flag"
	"log"
	"net/http"
	"strings"
	"sync"
	// "io/ioutil"
	// "encoding/json"
	"allowls/server"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"html/template"
)

//go:embed html/*
var html embed.FS

//go:embed templates/*
var templates embed.FS

//go:embed assets/*
var assets embed.FS

var (
	nodes       = flag.String("nodes", "", "comma seperated list of nodes")
	address     = flag.String("address", ":4001", "http host:port")
	rpc_address = flag.String("rpc_address", ":4441", "http host:port")
	load        = flag.Bool("load", false, "this loads the local storage into the local db on boot")
	store       = flag.String("store", "ipdb.dat", "Data file for storage")
	// global server
	srv *server.Server
)

type ContactList struct {
	AllowList []string `json:"allowlist"`
}

type AllowListRequest struct {
	IP    string `json:"ip"`
	Token string `json:"token"`
}

var mutex = &sync.Mutex{}

var dataFile = "data.json"

var contactLimit = 1

var AllowList = []string{"127.0.0.1", "10.23.23.1"}

var secret = []byte("ssdkcnasodimcsoimd")

var authHashes = []string{
	"YzBkMzWmZTllMmY0YTgzOTZhOTc5ZDJi",
	"NGR9SJCNmEjhkN2VhZmVlNsdkjc4M2Vl",
	"dS9wsSMiCDJMjliOSJCQY1NTAkdc9mM2",
}

func init() {
	flag.Parse()
}

func Contains(list []string, item string) bool {
	for _, v := range list {
		if v == item {
			return true
		}
	}
	return false
}

func Remove(items []string, item string) []string {
	newitems := []string{}

	for _, i := range items {
		if i != item {
			newitems = append(newitems, i)
		}
	}
	return newitems
}

func AllIPsFromAllowList(c *gin.Context) {
	hash := c.Param("hash")
	if Contains(authHashes, hash) {
		val, err := srv.All()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		if val == nil {
			log.Println("nothing found")
			c.JSON(http.StatusNotFound, gin.H{"error": "Nothing Found"})
			return
		}
		// result := strings.Join(val[:], ",")
		c.JSON(http.StatusOK, gin.H{"ips": val})
		return
	}
	c.JSON(http.StatusUnauthorized, gin.H{"error": "not found"})
}

func AddIpToAllowList(c *gin.Context) {
	var AllowListRequest AllowListRequest
	hash := c.Param("hash")
	if Contains(authHashes, hash) {
		if err := c.ShouldBindJSON(&AllowListRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		ip := AllowListRequest.IP
		if AllowListRequest.IP == "current" {
			ip = c.ClientIP()
		}
		if err := srv.Set(ip, "allowlist"); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "added " + ip})
		return
	}
	log.Println("hash not found")
	c.JSON(http.StatusUnauthorized, gin.H{"error": "not found"})
}

func RemoveIPFromAllowList(c *gin.Context) {
	var AllowListRequest AllowListRequest
	hash := c.Param("hash")
	if Contains(authHashes, hash) {
		if err := c.ShouldBindJSON(&AllowListRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		ip := AllowListRequest.IP
		if AllowListRequest.IP == "current" {
			ip = c.ClientIP()
		}

		if err := srv.Delete(ip); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "removed " + ip})
		return
	}
	c.JSON(http.StatusUnauthorized, gin.H{"error": "not found"})
}

func MainServer() {
	var members []string
	if len(*nodes) > 0 {
		members = strings.Split(*nodes, ",")
	}

	// create new server
	s, err := server.New(&server.Options{
		Members: members,
		Address: *rpc_address,
		Load:    *load,
		DbFile:  *store,
	})

	if err != nil {
		log.Fatal(err)
	}

	// set global server
	srv = s

	r := gin.Default()
	r.Use(sessions.Sessions("mysession", sessions.NewCookieStore(secret)))
	templ := template.Must(template.New("").ParseFS(templates, "templates/*"))
	// r.Use(gin.Recovery())
	r.SetHTMLTemplate(templ)
	// r.StaticFS("/static/", http.FS(assets))
	public := r.Group("/api")
	public.POST("/register", Register)

	// Login and logout routes
	public.POST("/login", Login)
	public.GET("/logout", Logout)
	public.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})
	private := r.Group("/local")
	private.Use(AuthRequired)
	{
		private.GET("/ping", func(c *gin.Context) {
			session := sessions.Default(c)
			user := session.Get(UserKey)
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
				"user":    user,
				"rpc":     s.Address,
				"web":     *address,
			})
		})
		private.GET("/bqj/:hash/allowlist", AllIPsFromAllowList)
		private.POST("/bqj/:hash/allowlist", AddIpToAllowList)
		private.DELETE("/bqj/:hash/allowlist", RemoveIPFromAllowList)

		// private.POST("/bqj/:hash/blacklist", AddIPToBlackList)
		// private.DELETE("/bqj/:hash/blacklist", RemoveIPFromBlackList)
	}
	r.GET("/", HomeWeb)
	r.Run(*address)
}

func HomeWeb(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(UserKey)
	var ip string
	LoggedIn := false
	mockip := c.Query("mockip")
	if mockip != "" {
		ip = mockip
	} else {
		ip = c.ClientIP()
	}
	if user != nil {
		LoggedIn = true
	}
	c.HTML(http.StatusOK, "index.tmpl", gin.H{"ip": ip, "LoggedIn": LoggedIn, "User": user})
}
